
public class BinarySearch {
	
	
	
	
	public static int binarySearch(int arrayOfInt[],int key,boolean searchFirst)
	{
		int low=0;
		int high=arrayOfInt.length;
		int resultIndex=-1;
		
		while(low<=high)
		{
			int mid=(low+high)/2;
			if (arrayOfInt[mid]==key)
			{
				resultIndex=mid;
				if(searchFirst)
				{
					high=mid-1;
				}
				else
					low=mid+1;
			}
			else if (key<arrayOfInt[mid])
			{
				high=mid-1;
			}
			else
				low=mid+1;
		}
		return resultIndex;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int data[]={5,6,7,7,7,7,7,7,8,9};
		int startPosition=BinarySearch.binarySearch(data, 7, true);
		int endPosition=BinarySearch.binarySearch(data, 7, false);
		System.out.println((endPosition-startPosition)+1);

	}

}
