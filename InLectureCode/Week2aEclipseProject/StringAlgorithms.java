
public class StringAlgorithms {

	public static int BruteForceStringMatch(int T[], int n, int P[], int m)
	{
		for(int i=0; i<=n-m;i++)
		{
			int j=0;
			while(j<m && P[j]==T[i+j])
			{
				j=j+1;
			}
			if(j==m) return i;
		}
		return -1;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int T[]={1,2,3,4,5,6,7};
		int P[]={7,5,6};
		System.out.println(StringAlgorithms.BruteForceStringMatch(T, T.length, P, P.length));

	}

}
