
public class Rectangle {

  private int length;
  private int width; 
  
  public Rectangle(int l,int w)
  {
    length=l;
    width=w;
    
  }
  
  public void setWidth(int w)
  {
   width=w;
  }
  
  public void setHeight(int h)
  {
   length=h;
  }
  
  public int getArea()
  {
    return this.length*this.width;
  }
  
  
  public int getPerimeter()
  {
    return (2*this.length)+(2*width);
  }
  
  
}
