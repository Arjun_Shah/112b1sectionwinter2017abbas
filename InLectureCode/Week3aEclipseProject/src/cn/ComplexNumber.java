package cn;

public class ComplexNumber {

  // What is a Complex Number?
  private final double realPart;
  private final double imaginaryPart;

  public String toString() {
    return Double.toString(realPart) + "+" + Double.toString(imaginaryPart) + "i";

  }

  // Create a ComplexNumber using Cartesian coordinates.
  private ComplexNumber(double r, double i) {
    realPart = r;
    imaginaryPart = i;
  }

  // Create a ComplexNumber using Polar coordinates
  /*
   * private ComplexNumber(float m,float angle) { realPart=m*Math.cos(angle);
   * imaginaryPart=m*Math.sin(angle); }
   */

  public static ComplexNumber createComplexNumberUsingCartesian(double real, double imaginary) {
    return new ComplexNumber(real, imaginary);
  }

  public static ComplexNumber createComplexNumberUsingPolar(double modulus, double angle) {
    return new ComplexNumber(modulus * Math.cos(angle), modulus * Math.sin(angle));
  }

  // What are the things you can do with a ComplexNumber?
  public ComplexNumber addTwoComplexNumbers(ComplexNumber n1) {
    /*this.realPart = this.realPart + n1.realPart;
    this.imaginaryPart = this.imaginaryPart + n1.imaginaryPart;
    return this;*/
    
    double realPart=n1.realPart+this.realPart;
    double imaginaryPart=n1.imaginaryPart+this.imaginaryPart;
    return new ComplexNumber(realPart,imaginaryPart);
    
  }
}
