package driver;

import cn.ComplexNumber;

public class ComplexNumberDriver {

  public static void main(String[] args) {
    // TODO Auto-generated method stub

    // Create a ComplexNumber with real part =4.0, imaginary part=5.0
    // ComplexNumber n1=new ComplexNumber(4.0,5.0);
    ComplexNumber n1 = ComplexNumber.createComplexNumberUsingCartesian(4.0, 5.0);
    System.out.println(n1);
    // Could I have done this instead?
    float angle = (float) Math.PI / 2.0f;
    // ComplexNumber n2=new ComplexNumber(6.0f,angle);
    ComplexNumber n2 = ComplexNumber.createComplexNumberUsingPolar(6.0f, Math.PI / 2);
    System.out.println(n2);
    
    System.out.println(n1.addTwoComplexNumbers(n2));
    
    System.out.println(n1);
  }

}
