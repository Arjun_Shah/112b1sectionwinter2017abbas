
public class FindMaximumElementInArray {

  
  public int findMaximumElement(int[] array)
  {
    int i=1;
    int maxElement=array[0];
    
    /*Loop invariant must be true here*/
    while(i<=array.length)
    {
      /*Loop invariant must be true here*/
      
      if (array[i]>maxElement)
      {
        maxElement=array[i];
      }
      i++;
    }
    /*Loop invariant must be true here*/
    
    return maxElement;
  }
}
