
public class LoopInvariant {

  
  public static int maxValue(int[] array)
  {
    int maxElement=array[0];
    int i=1;
    while(i<array.length)
    {
      if(maxElement<array[i])
      {
        maxElement=array[i];
      }
      i++;
    }
    return maxElement;
    
  }
  public static void main(String[] args) {
    // TODO Auto-generated method stub
      int[] array={-1,2,3,4};
      System.out.println(LoopInvariant.maxValue(array));
      
  }

}
