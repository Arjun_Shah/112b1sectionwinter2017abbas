package a1;

import static org.junit.Assert.*;

import org.junit.Test;

public class BinarySearchVariantTest {


	@Test(timeout=1000)
	public void testProblem1SqrtOf36() {
	    int studentOutput=BinarySearchVariant.problem1(36);
	    assertEquals("Executed squareroot of 36",6,studentOutput);
	}
	@Test(timeout=1000)
	public void testProblem1SqrtOf39() {
	    int studentOutput=BinarySearchVariant.problem1(39);
	    assertEquals("Executed squareroot of 39",6,studentOutput);
	}
	@Test(timeout=1000)
	public void testProblem1SqrtOf48() {
	    int studentOutput=BinarySearchVariant.problem1(48);
	    assertEquals("Executed squareroot of 48",6,studentOutput);
	}
	@Test(timeout=1000)
	public void testProblem1SqrtOf1() {
	    int studentOutput=BinarySearchVariant.problem1(1);
	    assertEquals("Executed squareroot of 1",1,studentOutput);
	}
	@Test(timeout=1000)
	public void testProblem1SqrtOf145() {
	    int studentOutput=BinarySearchVariant.problem1(145);
	    assertEquals("Executed squareroot of 145",12,studentOutput);
	}


	


	@Test(timeout=1000)
	public void testProblem1SqrtOf2() {
	    int studentOutput=BinarySearchVariant.problem1(2);
	    assertEquals("Executed squareroot of 2",1,studentOutput);
	}

	@Test(timeout=1000)
	public void testProblem1SqrtOf3() {
	    int studentOutput=BinarySearchVariant.problem1(3);
	    assertEquals("Executed squareroot of 3",1,studentOutput);
	}

	@Test(timeout=1000)
	public void testProblem1SqrtOf4() {
	    int studentOutput=BinarySearchVariant.problem1(4);
	    assertEquals("Executed squareroot of 4",2,studentOutput);
	}
	

	@Test(timeout=1000)
	public void testProblem1SqrtOf5() {
	    int studentOutput=BinarySearchVariant.problem1(5);
	    assertEquals("Executed squareroot of 5",2,studentOutput);
	}

	@Test(timeout=1000)
	public void testProblem1SqrtOf6() {
	    int studentOutput=BinarySearchVariant.problem1(6);
	    assertEquals("Executed squareroot of 6",2,studentOutput);
	}
	
	@Test(timeout=1000)
	public void testProblem1SqrtOf7() {
	    int studentOutput=BinarySearchVariant.problem1(7);
	    assertEquals("Executed squareroot of 7",2,studentOutput);
	}

	@Test(timeout=1000)
	public void testProblem1SqrtOf8() {
	    int studentOutput=BinarySearchVariant.problem1(8);
	    assertEquals("Executed squareroot of 8",2,studentOutput);
	}
	@Test(timeout=1000)
	public void testProblem1SqrtOf9() {
	    int studentOutput=BinarySearchVariant.problem1(9);
	    assertEquals("Executed squareroot of 9",3,studentOutput);
	}


	@Test(timeout=1000)
	public void testProblem1SqrtOf10() {
	    int studentOutput=BinarySearchVariant.problem1(10);
	    assertEquals("Executed squareroot of 10",3,studentOutput);
	}


	@Test(timeout=1000)
	public void testProblem1SqrtOf1023() {
	    int studentOutput=BinarySearchVariant.problem1(1023);
	    assertEquals("Executed squareroot of 1023",31,studentOutput);
	}


	@Test(timeout=1000)
	public void testProblem1SqrtOf1024() {
	    int studentOutput=BinarySearchVariant.problem1(1024);
	    assertEquals("Executed squareroot of 1024",32,studentOutput);
	}


	@Test(timeout=1000)
	public void testProblem1SqrtOf1025() {
	    int studentOutput=BinarySearchVariant.problem1(1025);
	    assertEquals("Executed squareroot of 1025",32,studentOutput);
	}



	@Test(timeout=1000)
	public void test1Problem2() {
	    int array[]={11,12,13,14,15,16,17,18};
	    int studentOutput=BinarySearchVariant.problem2(array);
	    assertEquals("Executed on {11,12,13,14,15,16,17,18}",7,studentOutput);
	}
	

	
	@Test(timeout=1000)
	public void test2Problem2() {
	    int array[]={8};
	    int studentOutput=BinarySearchVariant.problem2(array);
	    assertEquals("Executed on {8}",0,studentOutput);
	}
	
	@Test(timeout=1000)
	public void test3Problem2() {
	    int array[]={-8};
	    int studentOutput=BinarySearchVariant.problem2(array);
	    assertEquals("Executed on {-8}",0,studentOutput);
	}
	
	
	@Test(timeout=1000)
	public void test4Problem2() {
	    int array[]={11,12,13,-7,-8,-6};
	    int studentOutput=BinarySearchVariant.problem2(array);
	    assertEquals("Executed on {11,12,13,-7,-8,-6}",2,studentOutput);
	}
	
	
	@Test(timeout=1000)
	public void test5Problem2() {
	    int array[]={0,100,80,70,60,50,40};
	    int studentOutput=BinarySearchVariant.problem2(array);
	    assertEquals("Executed on {0,100,80,70,60,50,40}",1,studentOutput);
	}

	@Test(timeout=1000)
	public void test6Problem2() {
	    int array[]={100,90,-1,-2,-3,-4,-5};
	    int studentOutput=BinarySearchVariant.problem2(array);
	    assertEquals("Executed on {100,90,-1,-2,-3,-4,-5}",0,studentOutput);
	}

	@Test(timeout=1000)
	public void test7Problem2() {
	    int array[]={11,12,10};
	    int studentOutput=BinarySearchVariant.problem2(array);
	    assertEquals("Executed on {11,12,10}",1,studentOutput);
	}
	
}
