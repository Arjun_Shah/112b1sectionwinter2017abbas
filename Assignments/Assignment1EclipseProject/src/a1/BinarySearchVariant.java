package a1;

/**
 * @author: ENTER YOUR FIRST AND LAST NAME HERE
 * @email: ENTER YOUR BU EMAIL HERE
 *
 *YOU MAY ADD EXTRA HELPER METHODS TO THIS CLASS.
 */
public class BinarySearchVariant {

  public static final int REMOVE_ME = 5;

  /**
   * @param a is an array of integers.
   * @return k that is the apex of the array.
   */
  public static int problem2(int[] a) {
    /*
     * You can change the body of this function in any way that you wish. However, you cannot change
     * the signature of the method i.e. public static int findK(int[] A)
     */



    /*
     * Remove the following line, only added to avoid compile time errors in the starter code
     * provided by the instructor.
     */
    return REMOVE_ME;

  }

  /**
   * @param n>=1 is the number that you like to find the square root of.
   * @return back an int representation of the calculated squareroot.
   */
  public static int problem1(int n) {
    /*
     * You can change the body of this function in any way that you wish. However, you cannot change
     * the signature of the method i.e. public static int findSquareRoot(int n).
     */



    /*
     * Remove the following line, only added to avoid compile time errors in the starter code
     * provided by the instructor.
     */
    return REMOVE_ME;
  }
}
