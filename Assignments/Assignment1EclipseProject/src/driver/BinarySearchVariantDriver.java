package driver;

import a1.BinarySearchVariant;

/**
 * @author: ENTER YOUR FIRST AND LAST NAME HERE
 * @email: ENTER YOUR BU EMAIL HERE
 *
 *DO NOT ADD ANY FUNCTIONS OR METHODS IN THIS CLASS. THIS CLASS
 *MUST ONLY CONTAIN YOUR MAIN FUNCTION. 
 */
public class BinarySearchVariantDriver {

  public static void main(String[] args) {

    /*
     * You are free to change the main function in any way that
     * you like to test your code.
     * The following three lines are just examples -- remove
     * or edit as you wish. This method is not graded.
     */
    int arrayOfInt[] = {1, 2, 3};
    System.out.println(BinarySearchVariant.problem1(25));
    System.out.println(BinarySearchVariant.problem2(arrayOfInt));

  }

}
