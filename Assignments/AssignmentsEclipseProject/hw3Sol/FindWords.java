package hw3Sol;
import java.util.LinkedList;

public class FindWords {

	/*
	 * Returns true if an only the string s
	 * is equal to one of the strings in dict.
	 * Assumes dict is in alphabetical order.
	 */
	private static boolean inDictionary(String [] dict, String s) {
		// TODO Implement using binary search
		// Use String's compareTo method to compare
		// two strings. Do not run compareTo more than
		// once on the same pair of strings during the search -- 
		// instead, run it once and save the result in a variable
		// if you need it again.
		// You may want to see https://docs.oracle.com/javase/8/docs/api/java/lang/String.html
		int begin = 0;
		int end = dict.length - 1;

		/* loop invariant: the element 
		 * I am looking for is somewhere between
		 * dict[begin] and dict[end], inclusive
		 * (if it is there at all)
		 */
		while (begin<=end) {
			int mid = (begin+end)/2;
			int res = dict[mid].compareTo(s);
			if (res == 0) {
				return true;
			}
			else if(res<0) {
				begin = mid+1;
			}
			else {
				end = mid -1;
			}
		}
		return false;
	}
	
	/*
	 * Returns all strings that appear
	 * as a consecutive horizontal or vertical sequence of letters
	 * (left-right, right-left, up-down, or down-up)
	 * in the array board and also appear in dict.
	 * Note that the same word may appear multiple times
	 * on the board, and will then be multiple times in 
	 * the returned array.
	 * 
	 * dict is assumed to be in alphabetical order
	 * board is assumed to be rectangular
	 */
	public static String[] search(String[] dict, char[][] board) {
		LinkedList<String> ret = new LinkedList<String>();
		int height = board.length;
		
		// Set width to 0 for an empty board,
		// and to the width for first line otherwise,
		// because we assume the board is a rectangle
		int width = board.length==0 ? 0 : board[0].length;
		
		// TODO Generate substrings of the board
		// and check if they are in the dictionary
		// by calling inDictionary (which you implement).
		// If they are, use ret.add to add them
		// to the array that gets returned.
		// Character.toString, String+char, and String+=char
		// can all be useful here.
		// You may want to see https://docs.oracle.com/javase/8/docs/api/java/lang/String.html
        // and https://docs.oracle.com/javase/8/docs/api/java/lang/Character.html
		//
		// TODO (10% of your grade): if your board
		// has height h and width w, how many strings
		// do you need to check using inDictionary
		// (assume that you do nothing to filter out
		// duplicates or, equivalently, that the board
		// is such that there are no duplicates)
		// ANSWER: 
		// EXPLANATION:
		for (int row = 0; row < height; row++) {
			for (int col = 0; col < width; col++) {
				String start = Character.toString(board[row][col]);
				if (inDictionary(dict, start)) ret.add(start);
				
				String s = start;
				for (int i = col+1; i<width; i++) {
					s+=board[row][i];
					if (inDictionary(dict, s)) ret.add(s);
				}
				
				s = start;
				for (int i = col-1; i>=0; i--) {
					s+=board[row][i];
					if (inDictionary(dict, s)) ret.add(s);
				}
				
				s = start;
				for (int i = row+1; i<height; i++) {
					s+=board[i][col];
					if (inDictionary(dict, s)) ret.add(s);
				}
				
				s = start;
				for (int i = row-1; i>=0; i--) {
					s+=board[i][col];
					if (inDictionary(dict, s)) ret.add(s);
				}
				
			}
		}
		// This line converts LinkedList<String> to String []
		return ret.toArray(new String[0]);
	}
}
