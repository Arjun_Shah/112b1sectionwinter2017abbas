package hw3;
import java.util.LinkedList;

public class FindWords {

	/*
	 * Returns true if an only the string s
	 * is equal to one of the strings in dict.
	 * Assumes dict is in alphabetical order.
	 */
	private static boolean inDictionary(String [] dict, String s) {
		// TODO Implement using binary search
		// Use String's compareTo method to compare
		// two strings. Do not run compareTo more than
		// once on the same pair of strings during the search -- 
		// instead, run it once and save the result in a variable
		// if you need it again.
		// You may want to see https://docs.oracle.com/javase/8/docs/api/java/lang/String.html
		return false;
	}
	
	/*
	 * Returns all strings that appear
	 * as a consecutive horizontal or vertical sequence of letters
	 * (left-right, right-left, up-down, or down-up)
	 * in the array board and also appear in dict.
	 * Note that the same word may appear multiple times
	 * on the board, and will then be multiple times in 
	 * the returned array.
	 * 
	 * dict is assumed to be in alphabetical order
	 * board is assumed to be rectangular
	 */
	public static String[] search(String[] dict, char[][] board) {
		LinkedList<String> ret = new LinkedList<String>();
		int height = board.length;
		
		// Set width to 0 for an empty board,
		// and to the width for first line otherwise,
		// because we assume the board is a rectangle
		int width = board.length==0 ? 0 : board[0].length;
		
		// TODO Generate substrings of the board
		// and check if they are in the dictionary
		// by calling inDictionary (which you implement).
		// If they are, use ret.add to add them
		// to the array that gets returned.
		// Character.toString, String+char, and String+=char
		// can all be useful here.
		// You may want to see https://docs.oracle.com/javase/8/docs/api/java/lang/String.html
        // and https://docs.oracle.com/javase/8/docs/api/java/lang/Character.html
		//
		// TODO (10% of your grade): if your board
		// has height h and width w, how many strings
		// do you need to check using inDictionary
		// (assume that you do nothing to filter out
		// duplicates or, equivalently, that the board
		// is such that there are no duplicates)
		// ANSWER: 
		// EXPLANATION OF THE ANSWER:

		// This line converts LinkedList<String> to String []
		return ret.toArray(new String[0]);
	}
}
