package hw2sol;

import java.util.Scanner;

public class RightmostPlayer extends Player {

	public RightmostPlayer(int t) {
		super(t);
	}

	@Override
	int nextMove(Board board) {
		return board.rightmostNonEmptyColumn();
	}

}
