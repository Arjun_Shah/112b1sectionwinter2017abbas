package hw2sol;

public class Board {
	// 0 means empty, 1 means X, 2 means O
	public static final char [] sym = {' ', 'X', 'O'};
	private int [][] board;
	private int height, width;
	private int winLength;
	
	public Board (int h, int w, int winLen) {
		height = h;
		width = w;
		winLength = winLen;
		board = new int [height][width];
		// Java initializes new array elements to 0 automatically, so the board is empty
	}
	
	public int getWidth() {return width;}
	
	public void addChecker (int type, int column) {
		assert (type == 1 || type == 2);
		board[columnHeight(column)][column] = type;
	}
		
	public boolean canAddTo (int column) {
		return column>=0 && column<width && board[height-1][column]==0;
	}
	
	public boolean isFull () {
		for (int col = 0; col<width; col++) {
			if (board[height-1][col]==0) {
				return false;
			}
		}
		return true;
	}
	
	private int columnHeight(int col) {
		int row;
		// go from the top dow until find a full cell
		for (row = height - 1; row>=0 && board[row][col] == 0; row--) {
			;
		}
		return row+1;
	}
	
	/*
	 * returns -1 if all columns are full
	 * else returns the index of the column
	 * with the most checkers, excluding
	 * full columns, and breaking ties to the left 
	 */
	public int highestNonFullColumn() {
		int argMax = -1;
		int max = -1;
		for (int i=0; i<width; i++) {
			if (canAddTo(i)) {
				int h = columnHeight(i);
				if (h>max) {
					max = h;
					argMax = i;
				}
			}
		}
		return argMax;		
	}

	/*
	 * returns the index of the column with the fewest
	 * checkers, breaking ties to the left
	 */
	public int lowestNonEmptyColumn() {
		int argMin = 0;
		int min = columnHeight(0);
		for (int i=1; i<width; i++) {
			int h = columnHeight(i);
			if (h<min) {
				min = h;
				argMin = i;
				}
		}
		return argMin;		
	}

	public int leftmostNonEmptyColumn() {
		int i = 0;
		while (!canAddTo(i)) i++;
		return i;
	}

	public int rightmostNonEmptyColumn() {
		int i = width-1;
		while (!canAddTo(i)) i--;
		return i;
	}

	
	private boolean hasWonVertically (int type) {
		if(height < winLength) return false;
		for (int j = 0; j<width; j++) {
			int count = 0;
			for (int i = 0; i<height; i++) {
				if (board[i][j]==type) {
					count ++;
					if (count == winLength) return true;
				}
				else if (board[i][j]==0) break;
				else count = 0;
			}
		}
		return false;
	}
	
	private boolean hasWonHorizontally (int type) {
		if(width < winLength) return false;
		for (int i = 0; i<height; i++) {
			int count = 0;
			for (int j = 0; j<width; j++) {
				if (board[i][j]==type) {
					count++;
					if (count == winLength) return true;
				}
				else count = 0;
			}
		}
		return false;
	}
	
	private boolean hasWonNW (int type) {
		if(height < winLength || width < winLength) return false;
		int startRow = 0;
		int startCol = winLength-1;
		// try diagonals that start anywhere on the bottom edge
		// or on the right edge and are long enough
		while (startRow <= height-winLength) {
			int count = 0;
			for (int j = 0; startRow+j<height && startCol-j>=0; j++) {
				if (board[startRow+j][startCol-j] == type) {
					count++;
					if (count == winLength) return true;
				}
				else count = 0;
			}
			if (startCol==width-1) startRow++;
			else startCol++;
		}
		return false;
	}

	private boolean hasWonNE (int type) {
		if(height < winLength || width < winLength) return false;
		int startRow = height-winLength;
		int startCol = 0;
		// try diagonals that start anywhere on the left edge
		// or on the bottom edge and are long enough
		while (startCol <= width-winLength) {
			int count = 0;
			for (int j = 0; startRow+j<height && startCol+j<width; j++) {
				if (board[startRow+j][startCol+j] == type) {
					count++;
					if (count == winLength) return true;
				}
				else count = 0;
			}
			if (startRow==0) startCol++;
			else startRow--;
		}
		return false;
	}

	
	public boolean hasWon (int type) {
		return hasWonVertically (type) || hasWonHorizontally(type) || hasWonNE(type) || hasWonNW(type);
	}
	
	
	public String toString() {
		String s = "";
		for (int i = height-1; i>=0; i--) {
			for (int j = 0; j<width; j++) {
				s += "|"+sym[board[i][j]];
			}
			s+="|\n";
		}
		for (int j = 0; j<width; j++) {
			s += "--";
		}
		s+="-\n";
		for (int j = 0; j<width; j++) {
			s += " "+j%10;
		}
		s+="\n";
		return s;
	}

}
