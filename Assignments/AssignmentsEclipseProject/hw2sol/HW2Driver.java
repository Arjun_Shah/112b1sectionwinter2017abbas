package hw2sol;

class HW2Driver {
	public static void main (String [] args) {
		// To test your code, experiment with other players
		// you implement. Of course, before experimenting with those
		// other players, test code as you write it.
		Player p1 = new HighestColumnPlayer(1);
		Player p2 = new LeftmostPlayer(2);
		String [] message = {"Tie", "X wins", "O wins"};
		System.out.println(message[Game.play(5, 7, 5, p1, p2)]);
		
	}
}
