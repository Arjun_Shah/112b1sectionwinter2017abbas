package hw2sol;

import java.util.Scanner;

public class HighestColumnPlayer extends Player {

	public HighestColumnPlayer(int t) {
		super(t);
	}

	@Override
	int nextMove(Board board) {
		return board.highestNonFullColumn();
	}

}
