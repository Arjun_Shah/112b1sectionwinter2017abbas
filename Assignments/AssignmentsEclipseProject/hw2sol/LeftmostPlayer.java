package hw2sol;

public class LeftmostPlayer extends Player {
	
	public LeftmostPlayer(int t) {
		super(t);
	}


	@Override
	int nextMove(Board board) {
		return board.leftmostNonEmptyColumn();
	}

}
