package hw2sol;

public abstract class Player {
	
	protected int type; 	// 1 for X, 2 for O
	public Player (int t) {
		type = t;
	}
	abstract int nextMove(Board board);
}
