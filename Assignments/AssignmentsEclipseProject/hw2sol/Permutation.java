package hw2sol;

class Permutation {
	private int [] p;
	public Permutation (int n) {
		p = new int [n];
		for (int i=0; i<n; i++) {
			p[i]=i;
		}
	}
	public void transpose (int i, int j) {
		int temp = p[i];
		p[i]=p[j];
		p[j]=temp;
	}
	public void composeInPlace(Permutation that) {
		for (int i=0; i<p.length; i++) {
			p[i] = that.p[p[i]];
		}
	}
	public Permutation compose(Permutation that) {
		Permutation ret = new Permutation(p.length);
		for (int i=0; i<p.length; i++) {
			ret.p[i] = that.p[p[i]];
		}
		return ret;
	}
	public Permutation invert () {
		Permutation ret = new Permutation(p.length);
		for (int i=0; i<p.length; i++) {
			ret.p[p[i]] = i;
		}
		return ret;
	}
	public void invertInPlace () {
		int [] q = new int [p.length];
		for (int i=0; i<p.length; i++) {
			q[p[i]] = i;
		}
		 p = q;
	}
	public boolean isIdentity () {
		for (int i = 0; i<p.length; i++) {
			if (i!=p[i]) 
				return false;
		}
		return true;
	}
	public String toString () {
		String ret = "";
		for (int i : p) ret+= i+" ";
		return ret;
	}
}
