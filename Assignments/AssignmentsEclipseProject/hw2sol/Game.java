package hw2sol;

public class Game {
	public static int play(int height, int width, int winLength, Player p1, Player p2) {
		Board b = new Board(height,width,winLength);
		System.out.println(b);
		while (!b.isFull()) {
			 System.out.println("X moves next.");
			 b.addChecker(1, p1.nextMove(b));
			 System.out.println(b);
			 if (b.hasWon(1)) return 1;
			 if (b.isFull()) return 0;
			 System.out.println("O moves next.");
			 b.addChecker(2, p2.nextMove(b));
			 System.out.println(b);
			 if (b.hasWon(2)) return 2;
		}
		return 0;
	}

}
