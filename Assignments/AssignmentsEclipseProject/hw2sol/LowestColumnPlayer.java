package hw2sol;

public class LowestColumnPlayer extends Player {

	public LowestColumnPlayer(int t) {
		super(t);
	}

	@Override
	int nextMove(Board board) {
		return board.lowestNonEmptyColumn();
	}

}
