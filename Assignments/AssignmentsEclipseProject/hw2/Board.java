package hw2;

/**
 * @author: ENTER YOUR FIRST AND LAST NAME HERE
 * @email: ENTER YOUR BU EMAIL HERE
 */
public class Board {
  // 0 means empty, 1 means X, 2 means O
  public static final char[] sym = {' ', 'X', 'O'};
  private int[][] board;
  private int height, width;
  private int winLength;

  public Board(int h, int w, int winLen) {
    height = h;
    width = w;
    winLength = winLen;
    board = new int[height][width];
    // Java initializes new array elements to 0 automatically, so the board is empty
  }

  public int getWidth() {
    return width;
  }

  public void addChecker(int type, int column) {
    // TODO Fill in, so that checker of a given type (1 or 2)
    // is added on top of the existing stack of checkers
    // in a given column
  }

  public boolean canAddTo(int column) {
    // TODO return whether a checker can be added to the column
    return true; // REMOVE THIS LINE
  }

  public boolean isFull() {
    // TODO Return whether the entire board is filled in
    // For full credit: do not scan the entire board or even just
    // the column tops. Instead, add a variable to the class
    // that tells you how many pieces you already have on the board
    // and consult that variable
    return true; // REMOVE THIS LINE
  }

  private boolean hasWonVertically(int type) {
    // TODO Return whether there are winLength checkers of the given type
    // consecutively in any single column
    return true; // REMOVE THIS LINE
  }

  private boolean hasWonHorizontally(int type) {
    // TODO Return whether there are winLength checkers of the given type
    // consecutively in any single row
    return true; // REMOVE THIS LINE
  }

  private boolean hasWonNW(int type) {
    // TODO Return whether there are winLength checkers of the given type
    // consecutively on a diagonal that is oriented SouthEast-NorthWest
    return true; // REMOVE THIS LINE
  }

  private boolean hasWonNE(int type) {
    // TODO Return whether there are winLength checkers of the given type
    // consecutively on a diagonal that is oriented SouthWest-NorthEast
    return true; // REMOVE THIS LINE
  }


  public boolean hasWon(int type) {
    // TODO return whether there are winLength checkers
    // of the given type consecutively vertically,
    // horizontally, or diagonally (hint: use previous four methods)
    return true; // REMOVE THIS LINE
  }


  // DO NOT MODIFY THIS METHOD
  public String toString() {
    String s = "";
    for (int i = height - 1; i >= 0; i--) {
      for (int j = 0; j < width; j++) {
        s += "|" + sym[board[i][j]];
      }
      s += "|\n";
    }
    for (int j = 0; j < width; j++) {
      s += "--";
    }
    s += "-\n";
    for (int j = 0; j < width; j++) {
      s += " " + j % 10;
    }
    s += "\n";
    return s;
  }

}
