package hw2;

import java.util.Scanner;

public class HumanPlayer extends Player {

  private Scanner sc;

  public HumanPlayer(int t) {
    super(t);
    sc = new Scanner(System.in);
  }

  @Override
  int nextMove(Board board) {
    System.out.print(Board.sym[type] + ", your move? ");
    int move;
    do {
      move = sc.nextInt();
    } while (move >= board.getWidth());
    // TODO: currently, this checks only that the column
    // is not too high. Modify to check for other
    // bad inputs (column negative or full).
    // Do not remove, add, or change any print statements
    return move;
  }
}
