package hw2;

class HW2Driver {
  public static void main(String[] args) {
    // To test your code, experiment with other players
    // you implement. Of course, before experimenting with those
    // other players, test code as you write it.
    Player p1 = new HumanPlayer(1);
    Player p2 = new HumanPlayer(2);
    Game.play(6, 7, 4, p1, p2);
    // TODO this is where you can test what you create
    // and have fun--e.g., pair up a LeftmostPlayer and a
    // HighestColumnPlayer and see who wins.
    // This file is not graded.
  }
}
