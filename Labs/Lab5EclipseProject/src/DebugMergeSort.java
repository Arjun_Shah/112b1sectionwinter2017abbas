public class DebugMergeSort {
	

		private static int [] merge (int [] a1, int [] a2) {
			int [] ret = new int [a1.length+a2.length];
			int i = 0, j = 0, k = 0;
			while (i<a1.length && j<a2.length) {
				if (a1[i]<a2[j]) {
					ret[k] = a1[i++];
				}
				else {
					ret[k] = a2[j++];
				}
			}
			//	Only one of the two loops below will run: the first one
			//  if a1 is done, and the second one if a2 is done
			while (j<a2.length) {
				ret[k] = a2[j++];
			}
			while (i<a1.length) {
				ret[k] = a1[i++];
			}
			return ret;
		}
		/*
		 * Sorts the elements on a[begin ... end-1]
		 * Does not modify a -- instead returns a new array with the
		 * elements sorted
		 */
		public static int[] mergeSort (int [] a, int begin, int end) {
			if (begin == end-1) { // base case: one element array
				int [] ret = new int [1];
				ret[0] = a[begin];
				return ret;
			} else if (begin >= end) { // base case: empty array
				return new int [0];
			} else { // recursive case
				int mid = (begin+end)/2; // recursive case
				int [] a1 = mergeSort (a, begin, mid);
				int [] a2 = mergeSort (a, mid, end);
				return merge(a1, a2);
			}
		}
		
		public static int [] mergeSort (int [] a) {
			return mergeSort (a, 0, a.length);
		}
		
		public static String arrayToString(int [] a)
		{
			if(a.length == 0) return "[]";
			else
				{	
					String ret = "[" + a[0];
					for(int i = 1; i < a.length; i++)
					{
						ret+="," + a[i];
					}
					ret += "]";
					return ret;
				}
			
		}
		
		public static void main(String[] args) {

			int [] a = new int[]{1,44,201,20,44,199,44,2,49,10,23};
			int [] a1 = new int[]{0};
			int [] a2 = new int[]{};
			int [] a3 = new int[]{20,19,18,17,16,15,14,13,12,11,
					10,9,8,7,6,5,4,3,2,1};
			
			System.out.println("Input:" + arrayToString(a));
			a = mergeSort(a);
			System.out.println("Output:" + arrayToString(a));
			
			System.out.println("Input:" + arrayToString(a1));
			a1 = mergeSort(a1);
			System.out.println("Output:" + arrayToString(a1));
			
			System.out.println("Input:" + arrayToString(a2));
			a2 = mergeSort(a2);
			System.out.println("Output:" + arrayToString(a2));
			
			System.out.println("Input:" + arrayToString(a3));
			a3 = mergeSort(a3);
			System.out.println("Output:" + arrayToString(a3));
			
		
		}

	}


