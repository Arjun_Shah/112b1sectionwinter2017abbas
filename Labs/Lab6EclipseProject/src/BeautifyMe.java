
public class BeautifyMe {
    
    public static boolean isLessThanHundred (int val) {
        // refer to the hand out section on writing elegant code and fix this
        if((val < 100) == true) {
        return true;
        }else {
        return false;
        }}
    
    public static void main (String[] args) {
    int [][] a = {{0,5,10,99,20},{1,6,120},{121},{3,8,13,300,23,28}};
    int c = 0;  //give descriptive variable name
    for (int i=0; i < a.length; i++){
    for (int j=0; j < a[i].length; j++)  
    {
    if(isLessThanHundred(a[i][j]) == true){
    c++;
    }
    }
    }
    System.out.println("Count of elements less than 100 in the array is " + c);
    }
}
